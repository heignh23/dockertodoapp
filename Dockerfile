FROM node:10-alpine

# Create app directory inside the container
WORKDIR /var/www/todoapp

# Copy all files from where the dockerfile is into workdir
COPY . .

# Install app dependencies defined in package.json
RUN npm install

# When running the container, Docker will be listening on this port
EXPOSE 8080

CMD [ "node", "server.js" ]